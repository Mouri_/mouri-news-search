import React, { Component } from "react";
import logo from "../logo.png";

export default class Header extends Component {
  render() {
    return (
      <div className="header">
        <img src={logo} alt="mouri logo" />
        <h1>News search</h1>
        <input
        value={this.props.value}
          onChange={this.props.search}
          placeholder="Search by keyword or author"
        ></input>
        <button onClick={this.props.load}>Search</button>
      </div>
    );
  }
}
