import React, { Component } from "react";

export default class News extends Component {
  render() {
    const news = this.props.news;
    if (
      !(
        news.title &&
        news.url &&
        news.author &&
        news.num_comments &&
        news.points &&
        news.created_at
      )
    ) {
      return null;
    }

    return (
      <div className="newsContainer">
        <div
        className="news"
          onClick={() => {
            window.location.assign(`${news.url}`);
          }}
        >
          <h4>{news.title}</h4>
          <p className="newsINfo">
            {news.points} Points | {news.num_comments} Comments | Author:{" "}
            {news.author} | Date: {news.created_at.substring(0, 10)}
          </p>
        </div>
        <div className="line"></div>
      </div>
    );
  }
}
