import React, { Component } from "react";
import Header from "./components/Header";
import News from "./components/News";
import loading from "./loading.gif";
import axios from "axios";
import InfiniteScroll from "react-infinite-scroll-component";

class App extends Component {
  state = {
    news: [],
    search: "",
    page: 0,
    previousSearch: "",
    results: "Yay! You have seen it all",
    loadingImgStyle: { display: "flex" },
    maxPage: 1,
  };

  componentDidMount() {
    const keyword = this.state.search;
    const page = this.state.page;
    axios
      .get(`https://hn.algolia.com/api/v1/search?query=${keyword}&page=${page}`)
      .then(res => res.data)
      .then(res => {
        this.setState({ maxPage: res.nbPages });
        this.setState({ page: page + 1 });
        this.setState({ news: this.state.news.concat(res.hits) });
      });
  }

  load() {
    const keyword = this.state.search.replace(" ", "%20");
    const page = 0;
    if (keyword === this.state.previousSearch) return;
    this.setState({ page: 0, maxPage: 1 });
    this.setState({ news: [] });
    this.setState({results: "Yay! You have seen it all"})
    this.setState({ previousSearch: keyword });
    axios
      .get(
        `https://hn.algolia.com/api/v1/search?query=${keyword}&tags=(story,author)&page=${page}`
      )
      .then(res => res.data)
      .then(res => {
        if (res.hits.length === 0) {
          this.setState({
            results: `Sorry no results found`,
          });
        }
        this.setState({ maxPage: res.nbPages });
        return res;
      })
      .then(res => {
        this.setState({ news: res.hits });
        this.setState({ page: page + 1 });
      });
  }

  search(event) {
    let val = event.target.value;
    const last = val[val.length - 1];
    const regex = /^[a-zA-Z0-9 ]*$/;
    val = val.trim();
    if (last === " " && val.length > 0) {
      val += " ";
    }
    if (val.match(regex)) {
      this.setState({ search: val });
    }
  }

  render() {
    return (
      <div>
        <Header
          value={this.state.search}
          search={this.search.bind(this)}
          load={this.load.bind(this)}
        />
        <InfiniteScroll
          dataLength={this.state.news.length}
          hasMore={this.state.page < this.state.maxPage}
          next={this.componentDidMount.bind(this)}
          endMessage={
            <p style={{ textAlign: "center" }}>
              <b>{this.state.results}</b>
            </p>
          }
          loader={
            <div className="loading">
              <img
                style={this.state.loadingImgStyle}
                src={loading}
                alt="Loading..."
              />
            </div>
          }
        >
          {this.state.news.map(item => (
            <News news={item} />
          ))}
        </InfiniteScroll>
      </div>
    );
  }
}
export default App;
